# Program: motoring.py
# Author: Adam Fuller
# Date: 1/6/2015
# Description: Control of a motor via a Beaglebone digital output.

class turner():

	"""
	1. Import this file.
	2. instantiate this class e.g. t = motoring.turner('P8_13')
	3. t.setup()
	4. t.turn(0.123)
	"""

	import Adafruit_BBIO.GPIO as GPIO
	import time

	def __init__(self, control_pin='P8_13'):

		self.control_pin = control_pin

	def __del__(self):

		self.GPIO.cleanup()

	def setup(self):

		self.GPIO.cleanup()

		self.GPIO.setup(self.control_pin, self.GPIO.OUT)

	def turn(self, duration):

		assert type(duration) is float

		self.GPIO.output(self.control_pin, True)

		self.time.sleep(duration)

		self.GPIO.output(self.control_pin, False)
