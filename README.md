# To start #

```console
# ipython
```
```pyconsole
In [1]: import motoring
In [2]: t = motoring.turner('P8_13') # Instantiate turner.
In [3]: t.setup() # Set the pin to an output.
In [4]: t.turn(0.123) # Set the pin high, wait, then set low.
```